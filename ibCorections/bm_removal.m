function [data] = bm_removal(f, pStart, pEnd)
%BM_REMOVAL calculates bulk motion removal between adjancent B scans in OCT
%data
%
% Input:
%
%   f      - group of frames OCT data (raw signal) at same position
%   pStart - starting point of region in which we implement bulk motion
%            removal
%   pEnd   - end point of region in which we implement bulk motion removal
%
% Output:
%
%   data   - group of frames OCT data (after removal of bulk motion) at
%   same position
%
% See also: BM_CORRECT

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

if nargin == 2
    
    pEnd = size(f,1);
    
elseif nargin == 1
    
    pStart = 1;
    pEnd   = size(f,1);

end

if size(f,3)<2
    disp('Wrong input');
end

F = fft(f);
aF = angle(F);
mF = abs(F);
ang = aF;
I    = 20*log10(mF);
sumI = sum(sum(sum(I(pStart:pEnd,:,:))));
parI = I(pStart:pEnd,:,:);

bm   = zeros(size(f,1), size(f,2), size(f,3)-1);
data = zeros(size(f));

for k = 1:(size(f,3)-1)
    tmp1 = aF(:,:,k+1) - aF(:,:,k);
    tmp2 = wrapToPi(tmp1);
    parTmp2 = tmp2(pStart:pEnd,:); 
    tmp3 = sum(parI(:,:,k).*parTmp2)/sumI;
    bm(:,:,k) = repmat(tmp3, (pEnd-pStart+1),1);
    aF(:,:,k+1) = wrapToPi(aF(:,:,k+1) - bm(:,:,k)); 
end

for j = 1:size(f,3)
    data(:,:,j) = ifft(mF(:,:,j).*exp(i.*aF(:,:,j)));
end
