function [data]=bm_correct(data)
%BM_CORRECT calculates bulk motion removal between adjancent A scans in OCT
%data
%
% Input:
%
%   data - one frame of spectral OCT data (raw signal)
%
% Output:
%
%   data - one frame of spectral OCT data (after removal of bulk motion)
%
% See also: BM_REMOVAL

mag_data=abs(fft(data));
ang_data=angle(fft(data));
dpbulk=zeros(size(data));

for(countn=1:size(data,2)-1)
    p1 = ang_data(:,countn);
    p2 = ang_data(:,countn+1);
    dp=wrapToPi(p2-p1);
%     histn=2*pi/(iqr(dp).*2.*length(dp)^(-1/3));
    histn=10;

    [xi,yi]=ash(dp,8,round(histn));
    [ii,jj]=max(yi);
    p2prime=wrapToPi(p2-xi(jj));
     ang_data(:,countn+1)=p2prime;
%    dpbulk(:,countn+1)=p2prime;
end
data=ifft(mag_data.*exp(i.*(ang_data)));