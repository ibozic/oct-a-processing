function B = dftB(f)
%DFTB calculate registration between images using M. Guizar dftregistration
%function. Reference frame is middle frame in volume.
%
% Input:
%
%   f - raw OCT data
%
% Output:
%
%   B - registered structural data
%
% See also: DFTBB, DFTREGISTRATION, IBDFTREGISTRATION, CORRB

% Author: I. Bozic
% Data:   07/17/2015
% Cleveland, Ohio, US

F = abs(fft(f));
F1 = F(1:1024,:,:);
F2 = fft2(F1);
ref = F2(:,:,3);

[~, tmp1] = dftregistration((ref), F2(:,:,1), 12);
[~, tmp2] = dftregistration((ref), F2(:,:,2), 12);
[~, tmp3] = dftregistration((ref), F2(:,:,3), 12);
[~, tmp4] = dftregistration((ref), F2(:,:,4), 12);
[~, tmp5] = dftregistration((ref), F2(:,:,5), 12);

B(:,:,1) = abs(ifft2(tmp1));
B(:,:,2) = abs(ifft2(tmp2));
B(:,:,3) = abs(ifft2(tmp3));
B(:,:,4) = abs(ifft2(tmp4));
B(:,:,5) = abs(ifft2(tmp5));