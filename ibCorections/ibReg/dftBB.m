function [dF, dS] = dftBB(f, SS, par)
%DFTBB function is upgraded version of dftB function. It calculate
%registration of original OCT data, and SpliSpectrum decomposited data.
%Whole volume is registered to first frame.
%
% Inputs:
%
%   f   - raw OCT data or structural OCT data. If the input is raw OCT data
%         other parameters SHOULD NOT be passed
%   SS  - SplitSpectrum data
%   par - upsampling parameter in registration
%
% Outputs:
%
%   dF  - registrated structural data
%   dS  - registrated SplitSpectrum data, it will be filled only in case 
%         when SS input is passed 
%
% See also: DFTB, DFTREGISTRATION, IBDFTREGISTRATION

% Author: I. Bozic
% Data:   07/17/2015
% Cleveland, Ohio, US

if nargin < 3
    F = abs(fft(f));
    mag = F(1:1024,:,:);
else
    mag = f;
end
[~,~,n] = size(f);
if nargin == 1
    par  = 12;
    M = padarray(mag,[0, 100]);
    M1  = fft2(M);
    for i = 2:n
        ref = M1(:,:,(i-1));
        reg = M1(:,:,i);
        [~,tmp1] = dftregistration(ref,reg,par);
        M1(:,:,i) = tmp1;
    end
    dF = abs(ifft2(M1));
    dS = 0;
    
elseif nargin == 2
    par = 12;
    M = padarray(mag,[0, 100]);
    S = padarray(SS,[0, 100]);
    M1  = fft2(M);
    S1  = fft2(S);
    for i = 2:n
        ref = M1(:,:,(i-1));
        reg = M1(:,:,i);
        [~,tmp1, tmp2] = dftregistration1(ref,reg,S1(:,:,i), par);
        M1(:,:,i) = tmp1;
        S1(:,:,i) = tmp2;
    end
    dF = abs(ifft2(M1));
    dS = abs(ifft2(S1));
    
else         
    M = padarray(mag,[0, 100]);
    S = padarray(SS,[0, 100]);
    M1  = fft2(M);
    clear M;
    S1  = fft2(S);
    clear S;
    for i = 2:n
%         ref = M1(:,:,(i-1));
%         reg = M1(:,:,i);
        [~,M1(:,:,i), S1(:,:,i)] = ibDFTregistration(M1(:,:,(i-1)),M1(:,:,i),S1(:,:,i), par);
%         M1(:,:,i) = tmp1;
%         S1(:,:,i) = tmp2;
    end
    dF = abs(ifft2(M1));
    clear M1
    dS = abs(ifft2(S1));
    clear S1
end
