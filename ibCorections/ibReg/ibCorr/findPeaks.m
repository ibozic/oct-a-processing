function peaks  = findPeaks(F,ref)
%FINDPEAKS calculate position of peaks of crosscorrelated data
%
% Inputs:
%
%   F   - structural data
%   ref - structural data reference frame
%
% Outputs:
%
%   peaks - position of crosscorrelation peak
%
% See also: ALIGNEDDATA, CORRB

% Author: I. Bozic
% Data:   07/17/2015
% Cleveland, Ohio, US

[m, n, l] = size(F);
peaks = zeros(l,2);
CROW = m;
CCOL = n;
k = 0;
for i = 1:l  
%     for j = 1:l
%         XR=normxcorr2(F(:,:,i),F(:,:,j)); % j
        XR=normxcorr2(ref, F(:,:,i)); % j
        [row, col] = find(ismember(XR, max(XR(:))));
%         peaks(j+k*l,1) = row-CROW; % j
%         peaks(j+k*l,2) = col-CCOL; % j
        peaks(i,1) = row-CROW; % j
        peaks(i,2) = col-CCOL; % j
%     end
%     k = k+1;
end

end