function [amplitude, angl] = corrB(data)
%CORRB calculate registration based on crosscorrelation function
%
% Inputs:
% 
%   data      - raw OCT signal
%
% Outputs:
%
%   amplitude - structural image registrated to middle frame
%   angl      - phase image registrated to middle frame
%
% See also: FINDPEAKS, ALIGNEDDATA, DFTB, DFTBB

% Author: I. Bozic
% Data:   07/17/2015
% Cleveland, Ohio, US

F_com = (fft(data));
F = abs(F_com(1:1024,:,:));
ang = angle(F_com);
A = ang(1:1024,:,:);
ref = F(:,:,ceil(size(F,3)/2));
peaks = findPeaks(F,ref);
[mH, aH] = alignedData(F, A, peaks);
amplitude = mH;
angl= aH;