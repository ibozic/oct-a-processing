function [alData, anData] = alignedData(F, A, peaks)
%ALIGNEDDATA calculate registrated data on basis of original image and
%peaks of crosscorrelation.
%
% Inputs:
%
%   F       - structural data
%   A       - phase data
%   peaks   - crosscorrelation peaks
%
% Outputs:
%
%   alData  - registrated structural data
%   an Data - registrated phase data
%
% See also: FINDPEAKS, CORRB

% Author: I. Bozic
% Data:   07/17/2015
% Cleveland, Ohio, US

[m, ~, l] = size(F);

alData = zeros(size(F));
anData = zeros(size(F));
for i = 1:l  
    if peaks(i,1)<0
        j = (1+abs(peaks(i,1))):m;
        k = j-abs(peaks(i,1));
        alData(j,:,i)=F(k,:,i);
        anData(j,:,i)=A(k,:,i);
%         Comented due to vectrorization
%         for j=(1+abs(peaks(i,1))):m
%             for o =1:n
%                 alData(j,o,i)=F(j-abs(peaks(i,1)),o,i);
%             end
%         end

    elseif peaks(i,1)>0
        j = 1:(m-peaks(i,1));
        k = j+peaks(i,1);
        alData(j,:,i)=F(k,:,i);
        anData(j,:,i)=A(k,:,i);
%        Comented due to vectrorization   
%         for j=1:(m-peaks(i,1))
%             for o =1:n
%                   alData(j,o,i)=F(j+peaks(i,1),o,i);
%             end
%         end
    else
        alData(:,:,i)=F(:,:,i);
        anData(j,:,i)=A(k,:,i);
    end

end