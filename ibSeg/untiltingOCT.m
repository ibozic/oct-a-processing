function tImg = untiltingOCT(img)
gW =  GaussBank(1800, 900, 20, 1);
k = 1:100:501;
x = 50:100:500;
xx = 1:500;
mX = zeros(1,length(k)-1);
ind = zeros(1,length(k)-1);
for i = 1:465
    tmp = img(1:1800,:,i);
    for j = 1:(length(k)-1)
        tmp1 = tmp(:, k(j):(k(j+1)-1));
        mTmp = mean(tmp1,2);
%         j;
        tmp2 = segmentRPE(tmp1, 5);
        tmp3 = conv(mTmp,gW,'same');
        [mX(j),~ ] = max(tmp2);
        [~,ind(j)] = max(tmp3);
    end
    tmp4 = max(mX,ind);
%     ind
%     size(ind)
%     size(x)
    p   = polyfit(x,tmp4,1);
    rpe = polyval(p,xx);
    tImg(:,:,i) = flattening(tmp, rpe);
end