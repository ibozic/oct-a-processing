function ind = REM(TD, E2, dirL, cf)
%REM 
% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

Td = 1-TD;
[~,n,k] = size(Td);
Y = zeros(30,n,k);
MedianY = zeros(1,k);
for j = 1:k
    for i = 1:n;
        Y(:,i,j) = Td(E2(1,i,j):(E2(1,i,j)+29),i,j);
    end
    MedianY(j) = median(median(Y(:,:,j)));
end

mY = mean(MedianY);
sY = std(MedianY);
th = mY+2*sY;
tt = 0:.2:5;
TH = repmat(th, length(tt));


stem(MedianY,'LineStyle','none',...
     'MarkerFaceColor','red',...
     'MarkerEdgeColor','red'); axis([0 5 0 0.5]); hold on; plot(tt, TH, 'b-.'); hold off;
set(gcf,'Visible','off')

s = [dirL '\REM_' num2str(cf/5)];
print(s,'-dpng')

ind = MedianY<=th;