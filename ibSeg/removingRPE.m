function rpeF = removingRPE(image, RPE, param)
%REMOVINGRPE remove number of pixels defined with parameter below RPE
%segmented level by zeroout this region in average structural image
%
% Inputs:
%
%   image - average structural image
%   RPE   - segmented RPE layer
%   param - number of pixels to be removed bellow segmented RPE
%
% Output:
%
%   rpeF  - average structural image with removed RPE region
%
% See also: RPE, FLATTENING, STARTPOINT, SEGMENTRPE

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

rpeF = image;
[~, ~, n] = size(image);
for i = 1:n
    for j = 1:n
        tmp = RPE(i,j);
        tmp1 = tmp+param;
        rpeF(tmp:tmp1,j,i) = 0;
    end
    j = 0;
end