function fImg = flattening(image, RPE)
%FLATTENING function do flattening of retina layers based on estimation of
%RPE layer
%
% Inputs:
%
%   image - average structural data
%   RPE   - estimation of RPE layer
%
% Output:
%
%   fImg  - flattened image 
%
% See also: RPE

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

Y = fix(RPE);
d = max(Y)-min(Y);

% tM = mean(image(:));
% tmpF = [tM * ones((d+10), size(image,2)); image(10:end,:)];
% % % tmpF = [image(500:(510+d),:); image(10:end, :)];
tmpF = padarray(image, [d, 0]);

fImg = zeros(size(image));

for i = 1:size(image,2)

    df = Y(i)-Y(1);
    fImg(:,i) = tmpF((d+df+1):(size(image,1)+d+df),i);
end
