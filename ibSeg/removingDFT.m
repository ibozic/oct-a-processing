function [G,M, points] = removingDFT(dF)
%REMOVINGDFT function remove registration of lateral motion of scans
%which can be seen as __________ of data. It is used only for estimation
%of RPE layer in image after registration
%
% Inputs:
% 
%   dF     - registrated structural data
%
% Outputs:
%
%   G      - axial registrated data without lateral registration
%   M      - lateral registration information
%   points - two colume matrix with structural data points - starting point 
%            (columne 1), and ending point (columne 2)
%
% See also DFTBB, IBDFTREGISTRATION, STARTPOINT, FLATTENING, RPE, REMOVERPE

% Author: I. Bozic
% Data:   07/17/2015
% Cleveland, Ohio, US

% constants
[m,~,k] = size(dF);

% Finding border points of each structural data scan in registretad data
% frame
start = zeros(k,1);
stop = zeros(k,1);
for i = 1:k
    t = sum(dF(:,:,i));
    mt  = mean(t);
    tmp = find(t>(mt/2));

    while (length(tmp) < 500)
        tmp = [tmp, tmp(end)+1];
    end

    if length(tmp)>500
        tmp = tmp(1:500);
    end

    start(i) = tmp(1);
    stop(i)  = tmp(end);
end
points = [start stop];

% Removing structural data from registrated data
G = zeros(m, 500, k);
for i = 1:k
G(:,:,i) = dF(:,start(i):stop(i),i);
end

% Lateral registration information
M = zeros(size(dF));
for i = 1:k
M(:,1:start(i), i) =  dF(:,1:start(i), i); 
M(:,stop(i):end,i) = dF(:,stop(i):end,i);
end