function E2 = segNFL(TD, magF)
%SEGNFL function estimate pixels in NFL layer of retina. This is important
%for estimation how good decorrelation split spectrum masks are.
%
% Inputs:
%
%   TD    - decorelation split spectrum masks for each adjancent B scans
%   magF  - average intensity structural data over B scans
%
% Output:
% 
%   E@    - Estimation of NFL layer
%
% See also: SPLITSPECTRUM, REM

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

[~, n, k] = size(TD);

M = repmat(magF,1,1,k);
F1 = TD.*M;

mF = mean(mean(mean(F1)));
F2 = F1 - mF;
maxF = max(F2)/k;
E = 90*ones(2,n,k);
for i = 1:k
    for j = 1:n
        B = find(F2(:,j,i)>maxF(:,j,i));
        C = sort(B);
        D = find(C>(C(1)+60));
        if (isempty(D))
            %disp('Usao')
            if j == 1 && i~= 1
                %disp('Usao1')
                G = E(1,n,i-1);
            elseif j == 1 && i ==1
                %disp('Usao2')
                G = E(1,n,i+1);
            else
                %disp('Usao3')
                G = round(sum(E(1,1:(j-1),i))/(j-1));
            end
            %disp('Usao4')
            F = C(1);
        else
            %disp('Usao5')
            G = C(1);
            F = C(D(1));
        end   
                   
        E(:,j,i) = [G F];
    end
end

meanE = fix(squeeze(mean(E(1,:,:))));
for j = 1:4
    for i = 1:498;
        if(E(1,i,j)<=meanE(j))
    
            h = [E(1,i), E(1,i+1), E(1,i+2)];
            H = max(h);
            E(1,i,j) = H;
        end
    end
end
E2 = medfilt1(E(1,:,:),31);