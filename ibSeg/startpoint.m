function [p, fM] = startpoint(f, par, sigma)
%STARTPOINT determines depth in frame in which it has maximal value of RPE,
%based on convolution with gaussian template, parameter par define region 
%of RPE and zeroout rest of frame
%
% Inputs:
%
%   f     - axial registrated structural image
%   par   - percent of pixel to left on both side of maximal point in depth
%   sigma - width of Gaussian template
%
% Outputs:
%
%   p     - points of maximal depth for each frame
%   fM    - localized RPE region
%
% See also: GAUSSBANK, IBDFTREGISTRATION, RPE, REMOVINGDFT, REMOVINGRPE

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

[m,n,k] = size(f);
fM = zeros(m,n,k);
p = zeros(1,k);
gW =  GaussBank(m, fix(m/2), sigma, 1);
for i = 1:k
    T = mean(f(:,:,i),2);
    fT = conv(T,gW,'same');
    [~,r] = max(fT);
    p(i) = r;
end


top =fix((1-par)*p);
bot =fix((1+par)*p);

% top =fix(p-par);
% bot =fix(p+par);

for i = 1:k
    fM(top(i):bot(i),:,i) = f(top(i):bot(i),:,i);
end

