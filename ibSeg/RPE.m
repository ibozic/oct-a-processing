function fRPE = RPE(iRPE)
%RPE smooth and correct any wrong first estimation RPE values
%
% Input:
%
%   iRPE - first estimation RPE layer
%
% Output
%
%   fRPE - Smoothen and corrected RPE
%
% See also: SEGMENTRPE, REMOVERPE

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

sRPE   = medfilt1(iRPE,31);
mRPE   = fix(mean(sRPE,2));
% sRPE = iRPE;


% for i = 1:100
%     if sRPE(i,1)<mRPE(i,1)
%         sRPE(i,1) = max(iRPE(i,:)); % general is median
%     end
% end

% Left for reference
% for i = 1:500
% for j = 1:500
% RPEth1(i,j) = min(RPEth2(i,j), iRPE(i,j));
% end
% end

fRPE = sRPE;

for i = 1:100
    for j = 1:499
        if (((fRPE(i,j)-fRPE(i,j+1))>10) || ((fRPE(i,j)-fRPE(i,j+1))<-2))
            fRPE(i,j+1) = fRPE(i,j);
        end
    end
end