function RPE = segmentRPE(fM, par)
%SEGMENTRPE calculate first estimation of RPE in localized RPE image. First
%we do denoising of image using average filter to remove influence of
%bright speckles, and after that calculate vertical gradient to
%characterize RPE level from rest of image
%
% Inputs:
%
%   fM  - structural image with RPE region only
%   par - size of average filter (default is 5)
%
% Output:
%
%  RPE - First aproximation of RPE
%
% See also: STARTPOINT, REMOVINGDFT, RPE, REMOVINGRPE

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

if nargin < 2
    par = 5;
end

[~,m,l] = size(fM);

w = fspecial('average',par);
u = imfilter(fM,w,'same');



if l ~= 1
    
    RPE = zeros(l,500);
    U = zeros(size(fM));
    for i = 1:l
        U(:,:,i) = conv2(u(:,:,i),[1;-1], 'same');
        [~,r] = max(U(60:end,:,i));
        RPE(i,:) = r;
    end

else



    U = conv2(u,[1;-1], 'same');
    [~,RPE] = max(U);

end