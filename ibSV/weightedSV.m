function wSV = weightedSV(F, dirL, cF, IBc, par, d0, n)
%WEIGHTEDSV is function that calculates Speckle Variance between B scans
%           using weighted algorithm and absolute difference between adjencent B scans
%             
%                     N-1             
% SV(i,j,k) = 1/N-1 * sum|F(i,j,k+1)-F(i,j,k))| * f(MI)
%                      1              
%
% where are N represent number of B scans taken from same position, and F
% represent of Fourier transform of raw OCT signal. f(MI) represent
% weighting function that compesate motion
%
% Params:
%
%   f    - Intensity OCT image
%   dirL - directory path for saving images
%   cF   - current frame in colume
%   iBC  - current group of B scans on same position
%
% Output:
%
%   wSV  - Calculated Speckle Variance scan from imput B scans
%
% See also: SPECKLEVARIANCE, REGULARSV, LOGARITMICSV, ADJANCENTSV, 
%           BM_REMOVAL, CORRB, DFTB, DFTBB, DATASAVING

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US


if nargin < 6
    d0 = 0.1;
    n  = 0.5;
end
if nargin < 7 
    n = 1;
end

[m,k,l] = size(F);
omag = zeros(m,k,l-1);
MI = zeros(m,k,l-1);
for i = 1:(l-1)
    omag(:,:,i) = abs(F(:,:,i+1)-F(:,:,i));
    MI(:,:,i) = ((F(:,:,i+1)-F(:,:,i)).^2)./(F(:,:,i+1).^2+F(:,:,i).^2);
end

Fomag = mean(omag,3);
D = mean(MI,3);

fD = (D/d0).^n;

wSV = Fomag.*fD;

% dataSaving(dirL, omag, cF, 1, par, IBc, wSV);




