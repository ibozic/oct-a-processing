function aSV = adjancentSV(F, dirL, cF, IBc, par)
%ADJANCENTSV is function that calculates Speckle Variance between adjancent
%            B scans
%             
%
%                     N-1             
% SV(i,j,k) = 1/N-1 * sum[F(i,j,k+1)-F(i,j,k))].^2
%                      1              
%
% where are N represent number of B scans taken from same position, and F
% represent of Fourier transform of raw OCT signal
%
% Inputs:
%
%   f    - Intensity OCT image
%   dirL - directory path for saving images
%   cF   - current frame in colume
%   iBC  - current group of B scans on same position
%
% Output:
%
%   aSV  - Calculated Speckle Variance scan from imput B scans
%
% See also: SPECKLEVARIANCE, REGULARSV, LOGARITMICSV, WEIGHTEDSV, 
%           BM_REMOVAL, CORRB, DFTB, DFTBB, DATASAVING  

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US


SV = zeros(size(F,1),size(F,2),size(F,3)-1);
for i = 1:(size(F,3)-1)
    SV(:,:,i) = (F(:,:,i+1)-F(:,:,i)).^2;
end
aSV = mean(SV,3);

% dataSaving(dirL, SV, cF, 1, par, IBc, aSV);
