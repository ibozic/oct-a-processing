function [ibMA, ibAvg] = ibVariance(f, dirL, cF, IBc)
%IBVARIANCE calculate speckle variance using SplitSpectrum Paper formula
%
% Inputs:
%
%   f     - Intensity OCT image
%   dirL  - directory path for saving images
%   cF    - current frame in colume
%   iBC   - current group of B scans on same position
%
% Output:
%
%   ibMA  - Calculated Variance mask from B scans at same position
%   ibAvg - Average intensity on same postion B scans
%
% See also: SPECKLEVARIANCE, SPLITSPECTRUM, DOMAG, UHS_OMAG, DFTBB,
%           DATASAVING  

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

image = dftBB(f);
[~,~,n] = size(image);

tmp1 = repmat(mean(image,3), 1,1,n);
tmp2 = image.^2; 
tmp3 = repmat(mean(tmp2, 3), 1,1,n);

SV = ((image-tmp1).^2)./tmp3;

ibMA = mean(SV,3);
ibAvg  = tmp1(:,:,1);

dataSaving(dirL, SV, cF, 1,'SS', IBc, ibMA);

