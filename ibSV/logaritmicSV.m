function [lSV, mlSV] = logaritmicSV(F, dirL, cF, IBc, par)
%LOGARITMICSV is function that calculates Speckle Variance between B scans
%             USING LOGARITHMIC COMPRESSION
%
%                    N                  N
% SV(i,j,k) = 1/N * sum[I(i,j,k)-1/N * sum(I(i,j,k))].^2
%                    1                  1
%
% where are N represent number of B scans taken from same position, and I
% represent logarithmic compression of Fourier transform of raw OCT signal
%
% Inputs:
%
%   f    - Intensity OCT image
%   dirL - directory path for saving images
%   cF   - current frame in colume
%   iBC  - current group of B scans on same position
%
% Output:
%
%   lSV  - Calculated Speckle Variance scan from imput B scans
%   mlSV - Masked Spackle Variance scan with mean intensity
%
% See also: SPECKLEVARIANCE, REGULARSV, ADJANCENTSV, WEIGHTEDSV, BM_REMOVAL
%           CORRB, DFTB, DFTBB, DATASAVING

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US


I = log10(F+1);
setAvg = mean(I,3);
setAvg=repmat(setAvg,[1,1,size(F,3)]);
SV=(I-setAvg).^2;
lSV = mean(SV,3);
mlSV = lSV.*mean(F,3);

dataSaving(dirL, SV, cF, 1, par, IBc, lSV);
