function rSV = regularSV(F, dirL, cF, IBc, par)
%REGULARSV is function that calculates Speckle Variance between B scans
%          without implementing direct approach
%
%                    N                  N
% SV(i,j,k) = 1/N * sum[F(i,j,k)-1/N * sum(F(i,j,k))].^2
%                    1                  1
%
% where are N represent number of B scans taken from same position, and F
% represent Fourier transform of raw OCT signal
%
% Inputs:
%
%   f    - Intensity OCT image
%   dirL - directory path for saving images
%   cF   - current frame in colume
%   iBC  - current group of B scans on same position
%
% Output:
%
%   rSV  - Calculated Speckle Variance scan from imput B scans
%
% See also: SPECKLEVARIANCE, LOGARITMICSV, ADJANCENTSV, WEIGHTEDSV, 
%             BM_REMOVAL, CORRB, DFTB, DFTBB, DATASAVING

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US


setAvg = mean(F,3);
setAvg=repmat(setAvg,[1,1,size(F,3)]);
SV=(F-setAvg).^2;
rSV = mean(SV,3);

%dataSaving(dirL, SV, cF, 1, par, IBc, rSV);
