function [SV, mSV] = speckleVariance(f, mode, bm, adj, dirL, cF, IBc, par)

%SPECKLEVARIANCE is parent function for clculating different Speckle
%                Variance intensity images.
%
% Inputss:
%
%   f    - Raw input spectrogram of OCT image
%   mode - Defines type of Speckle Variance calculation (default is 'r')
%          [r - regular, l - logaritmic compressed, a - adjancent
%           w - weighted]
%   bm   - Defines type of bulk motion removal algorithm (default is 'n')
%          [n - none, a - A scam, b - B scan]
%   adj  - Defines algorithm for adjancment position of B scans (default 'D')
%          [d - DFT, c - Crosscorrelation]
%   dirL - directory path for saving images
%   cF   - current frame in colume
%   iBC  - current group of B scans on same position
%
% Outputs:
%
%   SV   - Calculated Speckle Variance scan from imput B scans
%   mSV  - Masked Spackle Variance scan with mean intensity, only in
%          logarimic compressed calculation, otherwise it is zero
%
% See also: REGULARSV, LOGARITMICSV, ADJANCENTSV, WEIGHTEDSV, CORRB, DFTB
%           DFTBB, SPLITSPECTRUM, PHASEVARIANCE, DOMAG, UHS_OMAG

% Setup default parameters

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

if nargin < 4
    adj  = 'D';
end
if nargin < 3
    adj  = 'D';
    bm   = 'n';
end
if nargin < 2
    adj  = 'D';
    bm   = 'n';
    mode = 'r';
end

% Performing bulk removal
switch bm 
    case 'a'
        for i = 1:size(f,3)
            f(:,:,i) = bm_correct(f(:,:,i));
        end
    case 'b'
        [~,f] = bm_removal(f);
    case 'n'
        f=f;
    otherwise
        disp('Wrong parameter');
end

% Performing Registration
switch adj
    case 'd'
        F = dftB(f);
    case 'c'
        F = corrB(f);
    case 'n'
        F = f;
    otherwise
        disp('Wrong parameter');
end

% Performing Speckle Variance
switch mode
    case 'r'
        mSV = 0;
        SV = regularSV(F, dirL, cF, IBc, par);
    case 'l'
        [SV, mSV] = logaritmicSV(F, dirL, cF, IBc, par);
    case 'a'
        mSV = 0;
        SV = adjancentSV(F, dirL, cF, IBc, par);
    case 'w'
        mSV = 0;
        SV = weightedSV(F, dirL, cF, IBc, par);
    otherwise
        disp('Wrong parameter');
end
