function A = postProcessingSS(ss, mag, par)
%POSTPROCESSINGSS do tresholding of Split Spectrum mask and calculate
%product of thresholded mask with average intensity on that position.
%Obtain result represent split spectrum frame for this position
%
% Inputs:
%
%   ss  - decorelation split spectrum frame
%   mag - average intensity
%   par - setup treshold (treshold = par*mean(ss))
%
% Outputs:
%
%   A   - split spectrum frame
%
% See also: SPLITSPECTRUM

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

Mean = mean(mean(mean(ss(10:300,:,:))));
thM = par*Mean;
tSS = ss.*(ss> thM);

A = tSS.*mag;


    
