function gF = GaussBank(point, peaks, sigma, num)
%GAUSSBANK create several gaussian filters on specific postion with
%determined width
%
% Inputs:
%   point - total number of points
%   peaks - peaks positions on points line
%   sigma - width of Gaussian
%   num   - wanted number of peaks
%
% Output:
%
%   gF    - defined Gaussian filter
%
% See also: SPLITSPECTRUM

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

if nargin < 4
    num = 4;
end
if length(peaks)~=num
    errordlg('Number of specified peaks must be equal to number of filters','File Error','Input Error');
end
    
gF = zeros(num,point);

x = linspace(0, point, point);
for i = 1:num
    gF(i,:) = exp(-(x- peaks(i)) .^ 2 / (2 * sigma ^ 2));
end