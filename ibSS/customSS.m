% This matlab script allows to run different number of subspectrums

ibPeaks = 10:10:2048;
ibGF = GaussBank(2048, ibPeaks, 20, 204);
J = ibGF';
for i=1:204
    eval(['GF' num2str(i) '= repmat(J(:,i), 1, 500, 5);']);
    eval(['T' num2str(i) '= w.*GF' num2str(i) ';']);
    eval(['TT' num2str(i) '= dftB(T' num2str(i) ');']);
end


TD1 = 0;
TD2 = 0;
TD3 = 0;
TD4 = 0;

for i = 1:204
    eval(['TD1' '= TD1+ (TT' num2str(i) '(:,:,1).*TT' num2str(i) '(:,:,2))./((TT' num2str(i) '(:,:,1).^2)/2+(TT' num2str(i) '(:,:,2).^2)/2);']);
    eval(['TD2' '= TD2+ (TT' num2str(i) '(:,:,2).*TT' num2str(i) '(:,:,3))./((TT' num2str(i) '(:,:,2).^2)/2+(TT' num2str(i) '(:,:,3).^2)/2);']);
    eval(['TD3' '= TD3+ (TT' num2str(i) '(:,:,3).*TT' num2str(i) '(:,:,4))./((TT' num2str(i) '(:,:,3).^2)/2+(TT' num2str(i) '(:,:,4).^2)/2);']);
    eval(['TD4' '= TD4+ (TT' num2str(i) '(:,:,4).*TT' num2str(i) '(:,:,5))./((TT' num2str(i) '(:,:,4).^2)/2+(TT' num2str(i) '(:,:,5).^2)/2);']);
end

TD1 = TD1/204;
TD2 = TD2/204;
TD3 = TD3/204;
TD4 = TD4/204;
