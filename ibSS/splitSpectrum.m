function [ss, mag]  = splitSpectrum(f,GF1, GF2, GF3, GF4, dirL, cF, IBc)
%SPLITSPECTRUM calculate decorelation split spectrum mask for raw OCT data.
%Using following formula:
%
%
%    SS = 1 - 1/N-1 * 1/M * sum(sum(Ai*Ai+1))
%
%
% Inputs:
%
%   f    - raw OCT data
%   GF1  - First Gaussian filter
%   GF2  - Second Gaussian filter
%   GF3  - Third Gaussian filter
%   GF4  - Fourth Gaussian filter
%   dirL - directory path for saving images
%   cF   - current frame in colume
%   iBC  - current group of B scans on same position
%
% Outputs:
%
%   ss   - decorelation split spectrum mask
%   mag  - average intensity
%
% See also: GAUSSBANK, POSTPROCESSINGSS, SEGNFL, REM, SPECKLEVARIANCE,
%           PHASEVARIANCE, DOMAG, UHS_OMAG, DFTBB, DATASAVING

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

% Spliting Spectrum
T1 = f.*GF1;
T2 = f.*GF2;
T3 = f.*GF3;
T4 = f.*GF4;

% Intensity of each subspectrum

TT1 = dftBB(T1);
TT2 = dftBB(T2);
TT3 = dftBB(T3);
TT4 = dftBB(T4);

% Calculating Decorrelation coefficinets

TD11 = (TT1(:,:,1).*TT1(:,:,2))./((TT1(:,:,1).^2)/2+(TT1(:,:,2).^2)/2);
TD21 = (TT2(:,:,1).*TT2(:,:,2))./((TT2(:,:,1).^2)/2+(TT2(:,:,2).^2)/2);
TD31 = (TT3(:,:,1).*TT3(:,:,2))./((TT3(:,:,1).^2)/2+(TT3(:,:,2).^2)/2);
TD41 = (TT4(:,:,1).*TT4(:,:,2))./((TT4(:,:,1).^2)/2+(TT4(:,:,2).^2)/2);

TD1 = (TD11+TD21+TD31+TD41)/4;

TD12 = (TT1(:,:,2).*TT1(:,:,3))./((TT1(:,:,2).^2)/2+(TT1(:,:,3).^2)/2);
TD22 = (TT2(:,:,2).*TT2(:,:,3))./((TT2(:,:,2).^2)/2+(TT2(:,:,3).^2)/2);
TD32 = (TT3(:,:,2).*TT3(:,:,3))./((TT3(:,:,2).^2)/2+(TT3(:,:,3).^2)/2);
TD42 = (TT4(:,:,2).*TT4(:,:,3))./((TT4(:,:,2).^2)/2+(TT4(:,:,3).^2)/2);

TD2 = (TD12+TD22+TD32+TD42)/4;

TD13 = (TT1(:,:,3).*TT1(:,:,4))./((TT1(:,:,3).^2)/2+(TT1(:,:,4).^2)/2);
TD23 = (TT2(:,:,3).*TT2(:,:,4))./((TT2(:,:,3).^2)/2+(TT2(:,:,4).^2)/2);
TD33 = (TT3(:,:,3).*TT3(:,:,4))./((TT3(:,:,3).^2)/2+(TT3(:,:,4).^2)/2);
TD43 = (TT4(:,:,3).*TT4(:,:,4))./((TT4(:,:,3).^2)/2+(TT4(:,:,4).^2)/2);

TD3 = (TD13+TD23+TD33+TD43)/4;

TD14 = (TT1(:,:,4).*TT1(:,:,5))./((TT1(:,:,4).^2)/2+(TT1(:,:,5).^2)/2);
TD24 = (TT2(:,:,4).*TT2(:,:,5))./((TT2(:,:,4).^2)/2+(TT2(:,:,5).^2)/2);
TD34 = (TT3(:,:,4).*TT3(:,:,5))./((TT3(:,:,4).^2)/2+(TT3(:,:,5).^2)/2);
TD44 = (TT4(:,:,4).*TT4(:,:,5))./((TT4(:,:,4).^2)/2+(TT4(:,:,5).^2)/2);

TD4 = (TD14+TD24+TD34+TD44)/4;

% Groping decorrelation

TD(:,:,1) = TD1;
TD(:,:,2) = TD2;
TD(:,:,3) = TD3;
TD(:,:,4) = TD4;

% removing scans up/below 1std
% maA = dftBB(f);
% mag = mean(maA,3);
% 
% E2  = segNFL(TD, mag);
% ind = REM(TD, E2, dirL, cF);
% l = length(ind);
% j = 1;
% for i = 1:l
%     if ind(i)==1
%       td(:,:,j) = TD(:,:,i);
%     end
%     j = j+1;
% end

ss = 1-mean(TD,3);

dataSaving(dirL, TD, cF, 1,'SS', IBc, ss);


