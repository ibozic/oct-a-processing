function B = scanMovie(A,par)

if nargin < 2
    par = 5;
end

tmp = fix(par/2);

[m,n] = size(A);
B = zeros(m,n,m+n);
for i = 1:(m+n)
    B(:,:,i)=A;
end
for i = 1:m
    t = (i-tmp):(i+tmp);
    t1 = t(t>0);
    B(t1,:,i)=255;
end
for i = (m+1):(m+n)
    t = (i-tmp):(i+tmp);
    t = t-m;
    t1 = t(t>0);
    B(:,t1,i)=255;
end

mB = max(max(max(B)));
B = B/mB;