function stackWrite(outputFileName, img)
outputFileName = [outputFileName '.tif'];
l = size(img,3);
for i=1:l
    imwrite(img(:, :, i), outputFileName, 'WriteMode', 'append');
end