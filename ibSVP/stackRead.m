function Img = stackRead(fname)

info = imfinfo(fname);
num_images = numel(info);


for i = 1:num_images
    Img(:,:,i) = imread(fname, i);
end
