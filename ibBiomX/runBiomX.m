function [labeledMap, Features] = runBiomX(filein)
global GEN BxF IDX

IDX = 1;
GEN = 2;

% Reading input file
A = stackRead(filein);
A = A(5:150,:,:);

% Generate Vasculature map
[mag, fSV] = vasculatureMap(A, 0, 3);
FSV = mean(fSV,3);

% Determine ONH position
[xc,yc,r] = ONHparam(mag);

% Generate vasculature tree
skeleton  = vascularTree(FSV, 13, 82);

% Calculate branching points
mask = CreateMask(0);
[R3, C3, ~, ~] = branchPoints(skeleton, mask);
bPoints =  [R3; C3];
BxF.bPoints = bPoints;

% Seperate ONH ans vessels
[ONH, vasculature] = extractONHvessels(skeleton, bPoints, xc, yc, r);

% Labeling vasculature
p = zeros(size(ONH));
[m,n,k] = size(vasculature);
tmp1 = zeros(m,n,k);
for i = 1:k
    GEN = 2;
    tmp1(:,:,i) = labelingVasulature(vasculature(:,:,i), mask, xc, yc, p);
end

labeledMap = sum(tmp1,3) + ONH;
Features   = BxF;
