function skeleton  = vascularTree(avg_img, k, thr)

if nargin < 3
    thr = 82; % 95
elseif nargin < 2
    thr = 95; % 82
    k   = 13;
end

% removing artifacts
A = double(avg_img);
h = fspecial('average', k);
A = imfilter(A,h);
[fx, fy] = gradient(A);
F = A-abs(fy)+abs(fx);

% Binarization
F(F<thr) = 0; %95 %85_nonreg
F1 = im2bw(F);

% Generating tree
F2 = bwmorph(F1,'thick');
F3 = bwmorph(F2,'thin',Inf);
F33 = F3;

% Finding the biggest connected tree
CC = bwconncomp(F3,8);
numPixels = cellfun(@numel,CC.PixelIdxList);
[~,idx] = max(numPixels);
F3(CC.PixelIdxList{idx}) = 0;
skeleton = F33-F3;
