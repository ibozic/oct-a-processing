function [R3, C3, R4, C4] = branchPoints(img, mask)

tmp1 = zeros([size(img),18]);
R3 = [];
R4 = [];
C3 = [];
C4 = [];

for i = 1 : 18
    tmp1(:,:,i) = conv2(img, mask(:,:,i), 'same');
end

tmp2 = conv2(img,ones(3),'same');
tmp3 = tmp1(:,:,17:18);

u = 1;
v = 1;
for i = 1:16
    [row,col] = find(((tmp1(:,:,i)==4 & tmp2(:,:)==4)));
    idx1 = size(row,1);
    for o = 1:idx1
        R3(u+o) = row(o);
        C3(u+o) = col(o);
    end 
    u = u+idx1; 
end

for i = 1:2
    [row1,col1] = find(((tmp3(:,:,i)==5 & tmp2(:,:)==5)));
    idx2 = size(row1,1);
    for o = 1:idx2
        R4(v+o) = row1(o);
        C4(v+o) = col1(o);
    end 
    v = v+idx2;
end

R3(R3 == 0) = [];
C3(C3 == 0) = [];
R4(R4 == 0) = [];
C4(C4 == 0) = [];