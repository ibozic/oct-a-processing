function [mag, fSV] = vasculatureMap(img, prm, sv)
%vasculatureMap function that calculates vasculature map using wOMAG
%algorithm. Before or/and after calculating vasculature map user can apply
%dft registration. Function also allow saving of stractural and vasculature
%data
%
% Inputs:
%
%   img  - OCT volume
%   prm  - parameter that define where dft registration will be applied 
%           0 - no registration
%           1 - only before wOMAG calculation
%           2 - only after wOMAG calculation
%           3 - before and after wOMAG calculation
%   sv   - parameter that define which data should be saved as tiff stack
%           0 - no saving
%           1 - only vasculature data
%           2 - only structural data
%           3 - both data
%
% Output:
%
%   mag  - Stractural data
%   faSV - Vasculature data
%
% See also: WEIGHTEDSV, DFTBB  

% Author: Ivan Bozic
% Date:   01/22/16
% Cleveland, Ohio, US

if nargin < 3
    sv  = 0;
elseif nargin <2
    prm = 0;
    sv  = 0;
end

[m,n,~] = size(img);
k = 500;
par = 12;
tmp1 = zeros(m,n,k);
tmp2 = zeros(m,n,k);
j = 1;

switch prm
    case 0
        fSV = zeros(m,n,k);
        mag = zeros(m,n,k);
        for i = 5:5:2500
            tmp = img(:,:,(i-4):i);
            fSV(:,:,j) = weightedSV(tmp,[],[],[],[]);
            mag(:,:,j) = mean(tmp,3);
            j = j+1;
        end
        
    case 1
        fSV = zeros(m,n+200,k);
        mag = zeros(m,n+200,k);
        for i = 5:5:2500
            tmp = img(:,:,(i-4):i);
            M = fft2(padarray(tmp,[0, 100]));
            for o = 2:5
                [~,M(:,:,o)] = dftregistration(M(:,:,o-1),M(:,:,o),12);
            end
            tmp2 = abs(ifft2(M));
            fSV(:,:,j) = weightedSV(tmp2,[],[],[],[]);
            mag(:,:,j) = mean(tmp2,3);
            j = j+1;
        end
        
    case 2
        fSV = zeros(m,n+100,k);
        mag = zeros(m,n+100,k);
        for i = 5:5:2500
            tmp = img(:,:,(i-4):i);
            fSV(:,:,j) = weightedSV(tmp,[],[],[],[]);
            mag(:,:,j) = mean(tmp,3);
            j = j+1;
        end
        M = padarray(mag,[0, 100]);
        M1  = fft2(M);
        clear M;
        S = padarray(fSV,[0, 100]);
        S1  = fft2(S);
        clear S;
        for i = 2:K
            [~,M1(:,:,i), S1(:,:,i)] = ibDFTregistration(M1(:,:,(i-1)),M1(:,:,i),S1(:,:,i), par);
        end
        fSV = abs(ifft2(S1));
        clear S1;
        mag = abs(ifft2(M1));
        clear M1;
        
    case 3
%         fSV = zeros(m,n+200,k);
%         mag = zeros(m,n+200,k);
        for i = 5:5:2500
            tmp = img(:,:,(i-4):i);
            M = fft2(padarray(tmp,[0, 100]));
            for o = 2:5
                [~,M(:,:,o)] = dftregistration(M(:,:,o-1),M(:,:,o),12);
            end
            tmp2 = abs(ifft2(M));
            fSV(:,:,j) = weightedSV(tmp2,[],[],[],[]);
            mag(:,:,j) = mean(tmp2,3);
            j = j+1;
        end
        M = padarray(mag,[0, 100]);
        M1  = fft2(M);
        clear M;
        S = padarray(fSV,[0, 100]);
        S1  = fft2(S);
        clear S;
        for i = 2:k
            [~,M1(:,:,i), S1(:,:,i)] = ibDFTregistration(M1(:,:,(i-1)),M1(:,:,i),S1(:,:,i), par);
        end
        fSV = abs(ifft2(S1));
        clear S1;
        mag = abs(ifft2(M1));
        clear M1;
end

switch sv
    
    case 0
    case 1
        max1 = max(max(max(fSV)));
        vol1 = fSV/max1;
        dt = clock;
        formOut = 'dd-mmmm-yyyy_HH_MM';
        sDT = datestr(dt, formOut);
        s1 = ['ID_BmX_' sDT];
        stackWrite(s1, vol1);
    case 2
        max2 = max(max(max(mag)));
        vol2 = mag/max2;
        dt = clock;
        formOut = 'dd-mmmm-yyyy_HH_MM';
        sDT = datestr(dt, formOut);
        s2 = ['ID_STR_' sDT];
        stackWrite(s2, vol2);
    case 3
        max1 = max(max(max(fSV)));
        max2 = max(max(max(mag)));
        vol1 = fSV/max1;
        vol2 = mag/max2;
        dt = clock;
        formOut = 'dd-mmmm-yyyy_HH_MM';
        sDT = datestr(dt, formOut);
        s1 = ['ID_BmX_' sDT];
        s2 = ['ID_STR_' sDT];
        stackWrite(s1, vol1);
        stackWrite(s2, vol2);
end

