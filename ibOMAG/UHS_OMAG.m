function uhsOMAG = UHS_OMAG(f)
%UHS_OMAG calculate optical microangiography from OCT images between
%adjancent B scans
%
% Inputs:
%
%   f       - raw OCT data
%
% Outputs:
%
%   uhsOMAG - optical microangiography image
%
% See also: DOMAG, SPLITSPECTRUM, SPECKLEVARIANCE, PHASEVARIANCE

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

[m,n,k] = size(f);
tmp = zeros(m,n,k-1);
for i = 1:(k-1)
    tmp(:,:,i) = f(:,:,i+1)-f(:,:,i);
end

uhsOMAG = fft(tmp);