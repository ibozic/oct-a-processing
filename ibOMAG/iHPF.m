function H = iHPF(m,n,r,a)
%IHPF calculate Frequency domain HighPass Butterworth filter
%
% Inputs:
%
%   m, n - dimensions of the filter
%   r    - cut-off frequency of the filter
%   a    - order of the filter
%
% Output
%
%   H    - HighPass Butterworth filter mask
%
% See also: DOMAG

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

[x,y] = meshgrid(1:n, 1:m);
j = floor(m/2)+1;
k = floor(n/2)+1;
D = sqrt((x-x(j,k)).^2+(y-y(j,k)).^2);
bF = 1-1./(1+(D/r).^(2*a));

H = repmat(bF,1,1,12);