function dOMAG = DOMAG(f,r,a)
%DOMAG calculate Doppler optical microangiography from OCT images
%
% Inputs:
%
%   f     - raw OCT data
%   r     - HighPass Butterworth filter cut-off frequency
%   a     - HighPass Butterworth filter order
%
% Outputs:
%
%   dOMAG - Doppler optical microangiography image
%
% See also: IHPF, UHS_OMAG, SPLITSPECTRUM, SPECKLEVARIANCE, PHASEVARIANCE

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

[m,n,~] = size(f);
H = iHPF(m,n,r,a);

FT_T = ifftshift(fft(f,[],2),2);
fFT_T = FT_T.*H;
fFT_t = ifft(ifftshift(fFT_T,2), [], 2);
dOMAG = fft(fFT_t);