function [PV, fPV, tPV, ftPV, mPV, mfPV, mtPV, mftPV] = phaseVariance(f, s, dirL, cF, IBc, par)
%PHASEVARIANCE calculate DIFFERENT phase variance MASKS between adjancent 
%B scans
%
% Inputs:
%
%   f    - raw OCT data
%   s    - four bit flag for definition of which mask should be calculated
%          (0000 - 1111)
%   dirL - directory path for saving images
%   cF   - current frame in colume
%   iBC  - current group of B scans on same position
%   par  - define should we use bulk motion removal or not ('NOT', 'YES') 
%
% Outputs:
%
%   PV   - phase variance
%   fPV  - filtered phase variance
%   tPV  - thresholded phase variance
%   ftPV - filtered thresholded phase variance
%   PV   - masked phase variance
%   fPV  - masked filtered phase variance
%   tPV  - masked thresholded phase variance
%   ftPV - masked filtered thresholded phase variance
%
% See also: CALCULATEPV, DOMAG, UHS_OMAG, SPLITSPECTRUM, SPECKLEVARIANCE

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

[m,n,l] = size(f);
F = fftshift(fft(f),1);
aF = angle(F);
mF = abs(F);
ang = aF(1025:end,:,:);
mag = mF(1025:end,:,:);
magAvg = mean(mag,3);
PV    = 0;
fPV   = 0;
tPV   = 0;
ftPV  = 0;
mPV   = 0;
mfPV  = 0;
mtPV  = 0;
mftPV = 0;

if ~strcmp(par,'NO')
    disp('U');
    ang = bm_removal(ang);
end

phi = zeros(m,n,l-1);
for i = 1:(l-1)
    phi(:,:,i) = ang(:,:,i+1) - ang(:,:,i);
end

switch s
    case '0000'
        PV = calculatePV(phi, dirL, cF, IBc, par);
    case '0001'
        PV  = calculatePV(phi, dirL, cF, IBc, par);
        mPV = magAvg.*PV;
    case '0010'
        PV = calculatePV(phi, dirL, cF, IBc, par);
        tpv = mean(mean(PV));
        tPV = zeros(size(PV));
        for i = 1:size(tPV,1)
            for j = 1:size(tPV,2)
                if (PV(i,j)>tpv)
                    tPV(i,j) = PV(i,j);
                end
            end
        end
    case '0011'
        PV = calculatePV(phi, dirL, cF, IBc, par);
        tpv = mean(mean(PV));
        tPV = zeros(size(PV));
        for i = 1:size(tPV,1)
            for j = 1:size(tPV,2)
                if (PV(i,j)>tpv)
                    tPV(i,j) = PV(i,j);
                end
            end
        end
        mtPV = magAvg.*tPV;
    case '0100'
        PV = calculatePV(phi, dirL, cF, IBc, par);
        fPV = medfilt2(PV, [4, 3]);
    case '0101'
        PV = calculatePV(phi, dirL, cF, IBc, par);
        fPV = medfilt2(PV, [4, 3]);
        mfPV = magAvg.*fPV;
    case '0111'
        PV = calculatePV(phi, dirL, cF, IBc, par);
        mPV = magAvg.*PV;
        fPV = medfilt2(PV, [4, 3]);
        mfPV = magAvg.*fPV;
        tpv = mean(mean(PV));
        tPV = zeros(size(PV));
        for i = 1:size(tPV,1)
            for j = 1:size(tPV,2)
                if (PV(i,j)>tpv)
                    tPV(i,j) = PV(i,j);
                end
            end
        end
        mtPV = magAvg.*tPV;
        ftpv = mean(mean(fPV));
        ftPV = zeros(size(PV));
        for i = 1:size(ftPV,1)
            for j = 1:size(ftPV,2)
                if (fPV(i,j)>ftpv)
                    ftPV(i,j) = fPV(i,j);
                end
            end
        end
        mftPV = magAvg.*ftPV;
    case '1000'
        PV = calculatePV(phi, dirL, cF, IBc, par);
    case '1001'
        PV  = calculatePV(phi, dirL, cF, IBc, par);
        mPV = magAvg.*PV;
    case '1010'
        PV = calculatePV(phi, dirL, cF, IBc, par);
        tpv = mean(mean(PV));
        tPV = zeros(size(PV));
        for i = 1:size(tPV,1)
            for j = 1:size(tPV,2)
                if (PV(i,j)>tpv)
                    tPV(i,j) = PV(i,j);
                end
            end
        end
    case '1011'
        PV = calculatePV(phi, dirL, cF, IBc, par);
        tpv = mean(mean(PV));
        tPV = zeros(size(PV));
        for i = 1:size(tPV,1)
            for j = 1:size(tPV,2)
                if (PV(i,j)>tpv)
                    tPV(i,j) = PV(i,j);
                end
            end
        end
        mtPV = magAvg.*tPV;
    case '1100'
        PV = calculatePV(phi, dirL, cF, IBc, par);
        fPV = medfilt2(PV, [4, 3]);
    case '1101'
        PV = calculatePV(phi, dirL, cF, IBc, par);
        fPV = medfilt2(PV, [4, 3]);
        mfPV = magAvg.*fPV;
    case '1111'
        PV = calculatePV(phi, dirL, cF, IBc, par);
        mPV = magAvg.*PV;
        fPV = medfilt2(PV, [4, 3]);
        mfPV = magAvg.*fPV;
        tpv = mean(mean(PV));
        tPV = zeros(size(PV));
        for i = 1:size(tPV,1)
            for j = 1:size(tPV,2)
                if (PV(i,j)>tpv)
                    tPV(i,j) = PV(i,j);
                end
            end
        end
        mtPV = magAvg.*tPV;
        ftpv = mean(mean(fPV));
        ftPV = zeros(size(PV));
        for i = 1:size(ftPV,1)
            for j = 1:size(ftPV,2)
                if (fPV(i,j)>ftpv)
                    ftPV(i,j) = fPV(i,j);
                end
            end
        end
        mftPV = magAvg.*ftPV;
    otherwise 
        disp('Wrong parameter''s');
end