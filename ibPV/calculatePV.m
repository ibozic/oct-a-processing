function PV = calculatePV(phi, dirL, cF, IBc, par)
%CALCULATEPV calculate phase variance between adjancent B scans
%
% Inputs:
%
%   phi  - phase information from OCT data
%   dirL - directory path for saving images
%   cF   - current frame in colume
%   iBC  - current group of B scans on same position
%
% Outputs:
%
%   PV   - phase variance
%
% See also: PHASEVARIANCE, DATASAVING

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

N = size(phi, 3);
PhiAvg = sum(phi,3)/N;
setAvg=repmat(PhiAvg,[1,1,N]);
PV1=(phi-setAvg).^2;
PV = mean(PV1,3);

dataSaving(dirL, PV1, cF, 1, par, IBc, PV);

