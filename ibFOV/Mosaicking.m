

% Gabor filtering of strips

k = 5:5:50;
th = 0:10:180;

for i = 1: length(k)
    for j = 1:length(th)
        h(:,:,j) = conv2(QR', gaborFilter(k(i), th(j), 10),'full');
        disp('0')
    end
    disp('I')
    tmp = max(h,[],3);
    disp('II')
    h1(:,:,i) = tmp(1:500,1:700);
    disp('III')
    clear h;
end

h2 = sum(h1,3);