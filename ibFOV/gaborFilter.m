function gK = gaborFilter(k, theta, v, sig)
%GABORFILTER produce Gabor kernel of specifide size (k), in direction
%described by theta and on specific spatial frequency v
%
% Input:
%
% k     - size of square Gabor kernel
% theta - direction of a kernel
% v     - spatial frequency of filter
%
% Output:
%
% gK    - designed kernel 
%
% See also:

% Author: Ivan Bozic
% Date:   07/21/15
% Cleveland, Ohio, US

if nargin < 4
    sig = 2;
end


[y,x] = meshgrid(1:k,1:k);
mx =  x*cos(theta)+y*sin(theta);
my = -x*sin(theta)+y*cos(theta);

gK = exp(-(mx.^2/sig.^2 + my.^2/sig.^2)/2).*cos(v*mx);
