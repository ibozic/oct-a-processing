function data=run_reshape(data1,DopplerNum,LinePerFrame)
data=zeros(size(data1,1),size(data1,2),DopplerNum);
for(n = 1:DopplerNum+1)
    data(:,:,n) = data1(:,[n:DopplerNum+1:(DopplerNum+1)*LinePerFrame]);
end