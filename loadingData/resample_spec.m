function [datak,index_k] = resample_spec(data,specCoeff)
index_pixel = [1:size(data,1)];
index_lambda = specCoeff(1);
for(n=2:length(specCoeff))
    index_lambda = index_lambda+specCoeff(n).*index_pixel.^(n-1);
end
index_lambda = index_lambda.*1e-9;
index_k = linspace(2.*pi/index_lambda(1),2.*pi/index_lambda(end),size(data,1));
index_klin = 2.*pi./index_k;
datak=spline(index_lambda,data',index_klin)';