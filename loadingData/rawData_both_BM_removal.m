%% Initialization
 clear all;close all; clc;
 
 

%% Constants
[fileName, pathName]        = uigetfile(['*.ocu'], 'Select a .ocu file');
fullFilePath                = strcat(pathName,fileName);
% pathName='Test-01_OD_V_3x3_0_0056108.OCU';
%  fileName='C:\Users\laser\Documents\MATLAB\mycodes_OCT';
% fullFilePath = 'C:\Users\laser\Documents\MATLAB\mycodes_OCT\Test-01_OD_V_3x3_0_0056108.OCU';
fid                 = fopen(fullFilePath);

% Output image file information
% mkdir(pathName,fileName(1:end-4));  % create directory based on filename with removed extension
imagePath               = strcat(pathName,fileName(1:end-4),'\'); % define a write path using this new directory
imageExtension          = '.tiff';

%% Read file header
magicNumber         = fread(fid,2,'uint16=>uint16');
magicNumber         = dec2hex(magicNumber);
versionNumber       = fread(fid,1,'uint16=>uint16');
versionNumber       = dec2hex(versionNumber);

 

keyLength           = fread(fid,1,'uint32');
key                 = char(fread(fid,keyLength,'uint8'));
dataLength          = fread(fid,1,'uint32');
if (~strcmp(key','FRAMEHEADER'))
    errordlg('Error loading frame header','File Load Error');
end

headerFlag          = 0;    % set to 1 when all header keys read
while (~headerFlag)
    keyLength       = fread(fid,1,'uint32');
    key             = char(fread(fid,keyLength,'uint8'));
    dataLength      = fread(fid,1,'uint32');
    
    % Read header key information
    if (strcmp(key','FRAMECOUNT'))
        frameCount      = fread(fid,1,'uint32');
    elseif (strcmp(key','LINECOUNT'))
        lineCount       = fread(fid,1,'uint32');
    elseif (strcmp(key','LINELENGTH'))
        lineLength      = fread(fid,1,'uint32');
    elseif (strcmp(key','SAMPLEFORMAT'))
        sampleFormat    = fread(fid,1,'uint32');
    elseif (strcmp(key','DESCRIPTION'))
        description     = char(fread(fid,dataLength,'uint8'));
    elseif (strcmp(key','XMIN'))
        xMin            = fread(fid,1,'double');
    elseif (strcmp(key','XMAX'))
        xMax            = fread(fid,1,'double');
    elseif (strcmp(key','XCAPTION'))
        xCaption        = char(fread(fid,dataLength,'uint8'));
    elseif (strcmp(key','YMIN'))
        yMin            = fread(fid,1,'double');
    elseif (strcmp(key','YMAX'))
        yMax            = fread(fid,1,'double');
    elseif (strcmp(key','YCAPTION'))
        yCaption        = char(fread(fid,dataLength,'uint8'));
    elseif (strcmp(key','SCANTYPE'))
        scanType        = fread(fid,1,'uint32');
    elseif (strcmp(key','SCANDEPTH'))
        scanDepth       = fread(fid,1,'double');
    elseif (strcmp(key','SCANLENGTH'))
        scanLength      = fread(fid,1,'double');
    elseif (strcmp(key','AZSCANLENGTH'))
        azScanLength    = fread(fid,1,'double');
    elseif (strcmp(key','ELSCANLENGTH'))
        elScanLength    = fread(fid,1,'double');
    elseif (strcmp(key','OBJECTDISTANCE'))
        objectDistance  = fread(fid,1,'double');
    elseif (strcmp(key','SCANANGLE'))
        scanAngle       = fread(fid,1,'double');
    elseif (strcmp(key','SCANS'))
        scans           = fread(fid,1,'uint32');
    elseif (strcmp(key','FRAMES'))
        frames          = fread(fid,1,'uint32');
    elseif (strcmp(key','DOPPLERFLAG'))
        dopplerFlag     = fread(fid,1,'uint32');
    elseif (strcmp(key','CONFIG'))
        config          = fread(fid,dataLength,'uint8');
    else
        headerFlag      = 1;
    end         % if/elseif conditional
end             % while loop

%% Read frame data
% Initialize frames in memory, need to modify for mod(lineLength,2)~=0
imageData           = zeros(lineLength,lineCount,'uint16');
imageFrame          = zeros(lineLength/2,lineCount,'uint16');
if dopplerFlag == 1
    dopplerData     = zeros(lineLength,lineCount,'uint16');
    dopplerFrame    = zeros(lineLength/2,lineCount,'uint16');
end
% images = zeros (frameCount,lineCount);

fseek(fid,-4,'cof');            % correct for 4-byte keyLength read in frame header loop
currentFrame        = 1;
frameLines          = zeros(1,frameCount);  % for tracking lines/frame in annular scan mode

ibPeaks = [409, 818, 1227, 1636];
ibGF = GaussBank(2048, ibPeaks, 200, 4);     
ibCoun = 0;
 GF = 0;
for (currentFrame = 1:frameCount);
    frameFlag       = 0;        % set to 1 when current frame read
    
    keyLength       = fread(fid,1,'uint32');
    key             = char(fread(fid,keyLength,'uint8'));
    dataLength      = fread(fid,1,'uint32');
    
    if (strcmp(key','FRAMEDATA'))
        while (~frameFlag)
            keyLength       = fread(fid,1,'uint32');
            key             = char(fread(fid,keyLength,'uint8'));
            dataLength      = fread(fid,1,'uint32'); % convert other dataLength lines to 'uint32'
            
            % The following can be modified to have frame values persist
            % Need to modify to convert frameDataTime and frameTimeStamp from byte arrays to real values
            if (strcmp(key','FRAMEDATETIME'))
                frameDateTime   = fread(fid,dataLength/2,'uint16'); % dataLength/2 because uint16 = 2 bytes
                frameYear       = frameDateTime(1);
                frameMonth      = frameDateTime(2);
                frameDayOfWeek  = frameDateTime(3);
                frameDay        = frameDateTime(4);
                frameHour       = frameDateTime(5);
                frameMinute     = frameDateTime(6);
                frameSecond     = frameDateTime(7);
                frameMillisecond= frameDateTime(8);
            elseif (strcmp(key','FRAMETIMESTAMP'))
                frameTimeStamp  = fread(fid,1,'double'); % dataLength is 8 for doubles
            elseif (strcmp(key','FRAMELINES'))
                frameLines(currentFrame)    = fread(fid,1,'uint32');
            elseif (strcmp(key','FRAMESAMPLES'))
                imageData       = fread(fid,[lineLength,frameLines(currentFrame)],'uint16=>uint16');
            elseif (strcmp(key','DOPPLERSAMPLES'))
                dopplerData     = fread(fid,[lineLength,frameLines(currentFrame)],'uint16=>uint16');
            else
                fseek(fid,-4,'cof');                    % correct for keyLength read
                if (currentFrame <= frameCount)
                    %                     display(currentFrame)
                end
                frameFlag       = 1;
            end % if/elseif for frame information
        end % while (~frameFlag)
        
        % Frame subsets
        imageFrame  = imageData;
        %         imageFrame  = imageData(lineLength/2+1:end,:);
        if (dopplerFlag == 1)
            dopplerFrame = dopplerData;
            %             dopplerFrame = dopplerData(lineLength/2+1:end,:);
        end % if to check Doppler flag
        
        if (frameCount < 10)
            index = strcat(num2str(currentFrame),imageExtension);
        elseif (frameCount < 100)
            if (currentFrame < 10)
                index = strcat('0',num2str(currentFrame),imageExtension);
            else
                index = strcat(num2str(currentFrame),imageExtension);
            end % if for index for frameCount < 100
        elseif (frameCount < 1000)
            if (currentFrame < 100)
                index = strcat('0',num2str(currentFrame),imageExtension);
            elseif (currentFrame < 10)
                index = strcat('00',num2str(currentFrame),imageExtension);
            else
                index = strcat(num2str(currentFrame),imageExtension);
            end % if for index for frameCount < 100
        elseif (frameCount < 10000)
            if (currentFrame < 1000)
                index = strcat('0',num2str(currentFrame),imageExtension);
            elseif (currentFrame < 100)
                index = strcat('00',num2str(currentFrame),imageExtension);
            elseif (currentFrame < 10)
                index = strcat('000',num2str(currentFrame),imageExtension);
            else
                index = strcat(num2str(currentFrame),imageExtension);
            end % if for index for frameCount < 100
        end % if/elseif for index creation
        
        if (frameHour < 10)
            frameHourStamp = strcat('0',num2str(frameHour));
        else
            frameHourStamp = num2str(frameHour);
        end % if/else for frameHour < 10
        
        if (frameMinute < 10)
            frameMinuteStamp = strcat('0',num2str(frameMinute));
        else
            frameMinuteStamp = num2str(frameMinute);
        end % if/else for frameMinute < 10
        
        if (frameSecond < 10)
            frameSecondStamp = strcat('0',num2str(frameSecond));
        else
            frameSecondStamp = num2str(frameSecond);
        end % if/else for frameSecond < 10
        
        frameMillisecondStamp   = num2str(frameMillisecond);
        
        if (dopplerFlag == 1)
            %             imageStamp          = sprintf('intensity_%d.%d.%d.%d_',frameHour,frameMinute,frameSecond,frameMillisecond);
            %             dopplerImageStamp   = sprintf('doppler_%d.%d.%d.%d_',frameHour,frameMinute,frameSecond,frameMillisecond);
            imageStamp          = strcat('intensity_',frameHourStamp,'.',frameMinuteStamp,'.',frameSecondStamp,'.',frameMillisecondStamp);
            dopplerImageStamp   = strcat('doppler_',frameHourStamp,'.',frameMinuteStamp,'.',frameSecondStamp,'.',frameMillisecondStamp);
            imageName           = strcat(imageStamp,index);
            dopplerImageName    = strcat(dopplerImageStamp,index);
            %             imageName           = strcat('intensity_',index);
            %             dopplerImageName    = strcat('doppler_',index);
        else
            imageStamp          = sprintf('%d.%d.%d.%d_',frameHour,frameMinute,frameSecond,frameMillisecond);
            imageName           = strcat(imageStamp,index);
        end % if for image names
        
        file=fileName;
        ScanPts = lineLength;
        LinePerFrame = lineCount;
        DopplerNum=0;
        Pixel_Offset=0;
        FrameNum=100;
        file=fileName;

% % % % 4-8 
% % % %         
% % % % specCoeff=[780 ...
% % % %             -0.060473535353535 ...
% % % %             8.312190706935688e-10 ...
% % % %             9.817987878789703e-11 ...
% % % %             3.081764204680626e-17];
        
% % % % Hdisp=[0.2351e-9 1.483e-17 8.8597e-24];
% % % 3        
specCoeff=[800 ...
0.050705605385626 ...
-6.711598531466445e-07 ...
-1.845855330443158e-10 ...
3.476970939587092e-14];

Hdisp=[-3.255572548130837e-11 -5.919992848012908e-17 -1.0878e-25];
        
        x=linspace(-1,1,LinePerFrame);
        
         
%           if(currentFrame <= 960 && currentFrame >= 957);   

  
  
           
    
           data=pre_process(imageFrame,ScanPts,DopplerNum,LinePerFrame,specCoeff, Hdisp);
           a=bm_correct(data);
           imbuff(:,:,currentFrame)  = a;
           if mod(currentFrame,5) == 0
               b = bm_removal(imbuf(:,:,(currentFrame-4):currentFrame));
               imbuff(:,:,(currentFrame-4):currentFrame) = b;
           end



           
           
           currentFrame
%            imbuff(:,:,currentFrame-floor(currentFrame/5).*5,1+floor(currentFrame/5))=data;
           
%           imbuffer(:,:,currentFrame-956)=data; 
%         end
%         
% currentFrame    = currentFrame + 1;     % will increase to frameCount + 1
    end % frames while loop

end % volume while loop


%            main_img = sum(imbuffer,3)/num_frame; 
%            figure
%            imagesc(20.*log10(main_img(1:800,:)),[65,110]);
%            colormap(gray)

%  [decorimg] = myssada (imbuffer);
%  figure
%  imagesc(decorimg(1:1024,:),[0.001 0.7]);
%  colormap('gray')
%  
 
% imbuffmat(:,:,:,1)=imbuff(:,:,1:3:end);
% imbuffmat(:,:,:,2)=imbuff(:,:,2:3:end);
% imbuffmat(:,:,:,3)=imbuff(:,:,3:3:end);
% imbuffmat(:,:,:,4)=imbuff(:,:,4:5:end);
% imbuffmat(:,:,:,5)=imbuff(:,:,5:5:end);

% imbuff=imbuffmat;

% save([fileName(1:end-3) 'mat'],'imbuff','-v7.3');



   

%% Shutdown
fclose(fid);
% close(figure(1));   % pops up due to imwrite function
% clear all;