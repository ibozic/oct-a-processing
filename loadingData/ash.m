function [x,y]=ash(f,m,n)

y=zeros(m*n+m,m);
[yi,xi]=hist(f,n);
dx=mean(diff(xi));

ind=[];
for(count=1:n)
    ind=[ind repmat(count,[1,m])];
end

for(count=1:m)
    y(count:count+length(ind)-1,count)= yi(ind);
end
x=[xi(1):dx/m:xi(end)+2.*dx-dx/m];
y=sum(y,2);