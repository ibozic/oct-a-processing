function [spectrum_array_k, Hdisp] = HilbertDisp(datak,Hdisp,k)

zmat=zeros(size(Hdisp));
for(n=1:length(Hdisp))
    if(Hdisp(n)~=0)
        zmat(n)=1;
    end
end

if(sum(zmat)==length(Hdisp))
    spectrum_array_k=datak;
else
    disp_order=length(Hdisp);
    
    Hdatak = hilbert(datak);
    
    Zmag_mat = abs((Hdatak));
    Zphase_mat = angle((Hdatak));
    
    % Zmag = Zmag_mat(:,1:10:end,1);
     %Zphase = Zphase_mat(:,1:10:end,1);
    Zmag = Zmag_mat(:,1:1:end,1);
    Zphase = Zphase_mat(:,1:1:end,1);
    
    p_index = repmat((k-mean(k))',[1,size(Zmag,2)]);
    
    spacen = 100;
    seed_lo0 = -1e-9;
    seed_hi0 = 1E-9;
    mstd = 1E-4;
    
    Hdispt=Hdisp;
    Hdisp = zeros(1,disp_order);
    disp_orderm=[];
    for(nn=1:length(Hdispt))
        if(zmat(nn)==1)
            Hdisp(nn)=Hdispt(nn);
        else
            disp_orderm=[disp_orderm nn];
        end
    end
    w = waitbar(0);
    for(dnn = 1:length(disp_orderm))
        disp_order=disp_orderm(dnn);
        dn=disp_order;
        seed_lo = seed_lo0*1e-5^(dn-1);
        seed_hi = seed_hi0*1e-5^(dn-1);
        [seed_lo seed_hi];
        n = 1;
        while(n == 1)
            vecn = linspace(seed_lo,seed_hi,spacen);
            mtemp = [];
            for(nn = 1:spacen)
                Hdisp(dn) = vecn(nn);
                Zphase2 = Zphase;
                for(p = 1:disp_order)
                    Zphase2 = Zphase2-(Hdisp(p).*(p_index.^(p+1)));
                end
                spectrum_array_k = real(Zmag.*exp(-1i.*Zphase2));
                %spec_temp = fftshift(abs(fft(spectrum_array_k)),1);
                spec_temp = abs(fft(spectrum_array_k));
                %spec_temp=spec_temp(2600:3200,1:300);
                %spec_temp=spec_temp(500:1060,200:700);
                spec_temp=spec_temp(200:500,:);
                %             m1n = sum(sum(abs(spec_temp).^4));
                m1n=1./sum(sum(spec_temp>.5e4));
                if(m1n == Inf)
                    m1n = 0;
                end
                mtemp = [mtemp m1n];
                figure(1);
                subplot(2,1,1)
                plot(vecn(1:nn),smooth(mtemp./mean(mtemp)));
                title(num2str(dn));
                subplot(2,1,2)
                imagesc(20.*log10(spec_temp),[60,90]);
                colormap(gray)
                %plot((spec_temp));
                title(num2str(disp_order));
                pause(0.001);
            end
            [maxm,maxi] = max(smooth(mtemp));
            seed_lo = vecn(maxi-round(spacen/20));
            seed_hi = vecn(maxi+round(spacen/20));
            Hdisp(dn) = vecn(maxi);
            if(std(mtemp./mean(mtemp))<mstd^dn)
                n = 0;
                Zphase=Zphase2;
            end
        end
        waitbar(dn/disp_order,w);
    end
    for(hn=1:length(Hdisp))
        format long;
        Hdisp(hn)
    end
    close(w);
end