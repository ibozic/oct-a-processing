function [data]=pre_process(imageFrame,ScanPts,DopplerNum,LinePerFrame,specCoeff,Hdisp);

[data,k] = resample_spec(double(imageFrame),specCoeff);
data=run_reshape(data,DopplerNum,LinePerFrame);

sfilt=gaussmf([1:size(data,1)],[size(data,1)/1,size(data,1)/2])';
sfilt=gausswin(size(data,1),1.5);
% sfilt=hann(size(data,1));

data=repmat(sfilt,1,size(data,2)).*data;
dc = repmat(mean((data),2),[1,size(data,2),1]);
data = data-dc;

[spectrum_array_k,Hdisp] = HilbertDisp(data,Hdisp,k);

disp_order=length(Hdisp);
Hdatak = hilbert(data);
Zmag_mat = abs((Hdatak));
Zphase_mat = angle((Hdatak));
p_index = repmat((k-mean(k))',[1,size(Zmag_mat,2)]);
for(p = 1:disp_order)
    Zphase_mat = Zphase_mat-Hdisp(p).*p_index.^(p+1);
end
data = real(Zmag_mat.*exp(-i.*Zphase_mat));