function [IP1, IP2] = APT(im,im2,Rmax)

IP1 = zeros(Rmax,8*Rmax);
IP2 = zeros(Rmax,8*Rmax);

for i = 1:Rmax
    Ri = i;
    nTh = 8*Ri;
    for j = 1:nTh
        x = Rmax+Ri*cos(2*pi*j/nTh);
        if x == 0
            x = 1;
        end
        y = Rmax+Ri*sin(2*pi*j/nTh);
        if y == 0;
           y = 1;
        end
        if (x - floor(x)) ~= 0
            x1 = floor(x)+1;
            x2 = ceil(x);
        end
        if (y - floor(y)) ~= 0
            y1 = floor(y)+1;
            y2 = ceil(y);
        end
        
        if (((x - floor(x)) ~= 0) && ((y - floor(y)) ~= 0))
            I1 = im(x1,y1);
            I2 = im(x2,y2);
            II1 = im2(x1,y1);
            II2 = im2(x2,y2);
            I = (x - floor(x))*I1+(1 - x + floor(x))*I2;
            II = (x - floor(x))*II1+(1 - x + floor(x))*II2;
        elseif (((x - floor(x)) ~= 0) && ((y - floor(y)) == 0))
            I1 = im(x1,y);
            I2 = im(x2,y);
            II1 = im2(x1,y);
            II2 = im2(x2,y);
            I = (x - floor(x))*I1+(1 - x + floor(x))*I2;
            II = (x - floor(x))*II1+(1 - x + floor(x))*II2;
        elseif (((x - floor(x)) == 0) && ((y - floor(y)) ~= 0))
            I1 = im(x,y1);
            I2 = im(x,y2);
            II1 = im2(x,y1);
            II2 = im2(x,y2);
            I = (y - floor(y))*I1+(1 - y + floor(y))*I2;
            II = (y - floor(y))*II1+(1 - y + floor(y))*II2;
        else
            I = im(x,y);
            II = im2(x,y);
        end
        
        IP1(i,j) = I;%im(Rmax+Ri*cos(2*pi*j/nTh),Rmax+Ri*sin(2*pi*j/nTh));
        IP2(i,j) = II;%im2(Rmax+Ri*cos(2*pi*j/nTh),Rmax+Ri*sin(2*pi*j/nTh));
    end
end