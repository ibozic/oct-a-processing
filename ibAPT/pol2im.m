function [imr2,T,RHO]=pol2im(imp_fftshift,upsamplen)

%% Upsample image    
[MM,NN]=size(imp_fftshift);
imp_fftshift=padarray(imp_fftshift,[MM*(upsamplen-1) NN*(upsamplen-1)],0,'post');
[MM,NN]=size(imp_fftshift);

%% Parameterize dimensions
cdim=linspace(-upsamplen,upsamplen,MM);
rdim=linspace(-upsamplen,upsamplen,NN);
[CC,RR]=meshgrid(cdim,rdim);

%% Polar transform dimensions
rhodim=linspace(0,max(rdim),MM);
tdim=linspace(0,360*upsamplen,NN);

[T,RHO]=meshgrid(tdim,rhodim);
[T2,RHO2]=cart2pol(RR,CC);

%% Interpolate polar to cartesian
imr2=interp2(RHO',T',imp_fftshift,RHO2,rad2deg(T2-min(min(T2))),'spline',0);

%% 2D FFT
imr2=fft2(fftshift(imr2))';