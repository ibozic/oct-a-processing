function [imp,T,RHO]=im2pol(im,upsamplen)

%% Upsample image    
im=imresize(im,upsamplen);
[MM,NN]=size(im);

%% 2D FFT
% im=fft2(im);
% im_fftshift=fftshift(im);

%% Parameterize dimensions
cdim=linspace(-1,1,MM);
rdim=linspace(-1,1,NN);
[CC,RR]=meshgrid(cdim,rdim);

%% Polar transform dimensions
rhodim=linspace(0,max(rdim),MM);
tdim=linspace(0,360,NN);

[T,RHO]=meshgrid(tdim,rhodim);
[PX,PY]=pol2cart(deg2rad(T),RHO);

%% Interpolate cartesian to polar
imp=interp2(CC,RR,im',PY,PX,'spline',0)';
% imp_fftshift=interp2(CC,RR,im_fftshift',PY,PX,'spline',0)';