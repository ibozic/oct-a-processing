% close all
clear all

% for(rot=1:360)

    
rot=45;


im = imread('cameraman.tif');
im=double(im);
imorigin=im;

imdims=[100 100];

rdiff=(size(im,1)-imdims(1))/2;
cdiff=(size(im,2)-imdims(2))/2;

im=imcrop(im,[rdiff cdiff imdims(1) imdims(2)]);

im2=imrotate(imorigin,rot);
if(size(im2)~=size(im))
rdiff=(size(im2,1)-size(im,1)-1)/2;
cdiff=(size(im2,2)-size(im,2)-1)/2;
im2=imcrop(im2,[rdiff cdiff size(im,1)-1 size(im,2)-1]);
end

[imp1,T,RHO]=im2pol(im,1);
% [imr1,T,RHO]=pol2im(imp1,1);

[imp2,T,RHO]=im2pol(im2,1);
% [imr2,T,RHO]=pol2im(imp1,1);

figure(1)
subplot(2,2,1)
imagesc(im)
axis square
subplot(2,2,2)
imagesc(((abs(imr1))))
axis square
subplot(2,2,3)
imagesc(log10(abs(fftshift(fft2(im)))))
axis square
subplot(2,2,4)
imagesc(log10(abs(imp1)))
axis square
figure(2)
subplot(2,1,1)
imagesc(log10(abs(imp1)))
axis square
subplot(2,1,2)
imagesc(log10(abs(imp2)))
axis square
figure(3)
imshowpair(log10(abs(imp1)),log10(abs(imp2)))
figure(4)
plot(sum(abs(fftshift(fft(log10(abs(fftshift(ifft2(fft2(imp1).*conj(fft2(imp2))))))'))),1))

% impp1=

% imp1(:,1:10)=0;
% imp2(:,1:10)=0;

[a,b]=dftregistration((imp1),(imp2));
avec(:,rot)=a;
rot
% end

