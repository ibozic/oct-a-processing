function ppVol = regPreProcess(vol)

vol = double(vol);
mVol = mean(mean(mean(vol)));
sVol = std (std (std (vol)));

nVol = (vol - mVol) / sVol;

[m,n,k] = size(nVol);

tVol = zeros(m,n,k);

for i = 1:k
    for j = 1:n
        tVol(:,j,i) = medfilt1(nVol(:,j,i),5);
    end
end

tVol = tVol(200:(end-200),:,:);

[r,c,~] = size(tVol);

ppVol = padarray(tVol, [0, (r-c)/2]);