function [scale, rot] = parRotScale(ref, reg)

img1 = imresize(ref,4,'box');
img2 = imresize(reg,4,'box');

Rmax = length(img1)/2;

% Adaptive polar transform

[IP1, IP2] = APT(img1,img2,Rmax);

% Thetha Projections

[Theta1, Theta2] = projTheta(IP1,IP2,Rmax);

% Rotational angle estimation

T1 = fft(Theta1-mean(Theta1));
[~,ind1] = max(T1(1:length(T1)/2));
T2 = fft(Theta2-mean(Theta2));
[~,ind2] = max(T2(1:length(T2)/2));
rot = rad2deg(angle(T1(ind1)) - angle(T2(ind2)));

% Rho Projection

[Rho1, Rho2] = projRho(IP1,IP2,Rmax);

% Scaling parameter estimation

r1 = abs(fft(Rho1));
[~,ind1] = max(r1(1:length(r1)/2));
r2 = abs(fft(Rho2));
[~,ind2] = max(r2(1:length(r2)/2));
scale = r1(ind1)/r2(ind2);

