function [Rho1, Rho2] = projRho(IP1,IP2,Rmax)

l = size(IP1,1);
Rho1 = zeros(1, l);
Rho2 = zeros(1, l);
ntheta = 8*Rmax;

for i = 1:l
    tmp3 = 0;
    tmp4 = 0;
    nTh = 8*i;
    O = ntheta/nTh;
    for j = 1:nTh
        tmp3 = tmp3 + O*IP1(i,j);
        tmp4 = tmp4 + O*IP2(i,j);
    end
    Rho1(i) = tmp3;
    Rho2(i) = tmp4;
end