function [img,rot] = rotReg(ref, reg)


[scale, rot] = parRotScale(ref, reg);

% Rotational registration
% reg = imresize(reg, 1/scale);

% Rotational registrtion
if abs(rot) < 1.3
    img = reg;
%     rot
else
%     disp('Yes')
    tmp = imrotate(reg, -rot);
    [m,~] = size(tmp);
    if m ~= 700
        if mod(m,2)==0;
            c = m/2;
        else
            c = (m-1)/2;
        end
        img = tmp((c-349):(c+350), (c-349):(c+350));
    else
        img = tmp;
    end
end
