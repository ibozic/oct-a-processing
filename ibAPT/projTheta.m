function [Theta1, Theta2] = projTheta(IP1,IP2,Rmax)

Theta1 = zeros(1, size(IP1,2));
Theta2 = zeros(1, size(IP1,2));
ntheta = 8*Rmax;

for j = 1:ntheta
    tmp3 = 0;
    tmp4 = 0;
    for i = 1:Rmax
        nTh = 8*i;
        O = ntheta/nTh;
        n1 = O*(j-1)-floor(O*(j-1));
        n2 = 1-n1;
        tmp1 = ceil((j-1)/O);
        tmp2 = ceil(j/O);
        if tmp1 ~=0 && tmp2 ~= 0
            tmp3 = tmp3 + n1*IP1(i,tmp1)+n2*IP1(i,tmp2);
            tmp4 = tmp4 + n1*IP2(i,tmp1)+n2*IP2(i,tmp2);
        elseif tmp1 ~=0 && tmp2 == 0
            tmp3 = tmp3 + n1*IP1(i,tmp1);
            tmp4 = tmp4 + n1*IP2(i,tmp1);
        elseif tmp1 ==0 && tmp2 ~= 0
            tmp3 = tmp3 + n2*IP1(i,tmp2);
            tmp4 = tmp4 + n2*IP2(i,tmp2);
        else
            tmp3 = tmp3+0;
            tmp4 = tmp4+0;
        end
    end
    Theta1(j) = tmp3;
    Theta2(j) = tmp4;
end
