clear all;
clc;
close all;

im = imread('cameraman.tif');
im=double(im);
imorigin=im;
im1 = imresize(im,32,'box');
% im2 = ibReSize(img, 2);
% im = abs(ifft2(ifftshift(padarray(fftshift(fft2(im)), [128 128], 'both'))));

% imdims=[100 100];
% 
% rdiff=(size(im,1)-imdims(1))/2;
% cdiff=(size(im,2)-imdims(2))/2;

% im=imcrop(im,[rdiff cdiff imdims(1)-1 imdims(2)-1]);



rot = -1;

im2=imrotate(im,rot);


im = padarray(im, [floor((length(im2)-length(im))/2), floor((length(im2)-length(im))/2)]);
Rmax = length(im)/2;


% im = abs(fftshift(fft2(im)));
% im2 = abs(fftshift(fft2(im2)));
% im = original;
% im2 = distorted;


% im = imread('cameraman.tif');
% im=double(im);
% imorigin=im;
% 
% imdims=[100 100];
% 
% rdiff=(size(im,1)-imdims(1))/2;
% cdiff=(size(im,2)-imdims(2))/2;
% 
% im=imcrop(im,[rdiff cdiff imdims(1) imdims(2)]);
% 
% % im2=imrotate(imorigin,rot);
% im2 = imresize(im,2);
% if(size(im2)~=size(im))
% rdiff=(size(im2,1)-size(im,1)-1)/2;
% cdiff=(size(im2,2)-size(im,2)-1)/2;
% im2=imcrop(im2,[rdiff cdiff size(im,1)-1 size(im,2)-1]);
% end


[IP1, IP2] = APT(im,im2, Rmax);


% Rmax = length(im)/2;
% 
% for i = 1:Rmax
%     Ri = i;
%     nTh = 8*Ri;
%     for j = 1:nTh
%         x = Rmax+Ri*cos(2*pi*j/nTh);
%         if x == 0
%             x = 1;
%         end
%         y = Rmax+Ri*sin(2*pi*j/nTh);
%         if y == 0;
%            y = 1;
%         end
%         if (x - floor(x)) ~= 0
%             x1 = floor(x)+1;
%             x2 = ceil(x);
%         end
%         if (y - floor(y)) ~= 0
%             y1 = floor(y)+1;
%             y2 = ceil(y);
%         end
%         
%         if (((x - floor(x)) ~= 0) && ((y - floor(y)) ~= 0))
%             I1 = im(x1,y1);
%             I2 = im(x2,y2);
%             II1 = im2(x1,y1);
%             II2 = im2(x2,y2);
%             I = (x - floor(x))*I1+(1 - x + floor(x))*I2;
%             II = (x - floor(x))*II1+(1 - x + floor(x))*II2;
%         elseif (((x - floor(x)) ~= 0) && ((y - floor(y)) == 0))
%             I1 = im(x1,y);
%             I2 = im(x2,y);
%             II1 = im2(x1,y);
%             II2 = im2(x2,y);
%             I = (x - floor(x))*I1+(1 - x + floor(x))*I2;
%             II = (x - floor(x))*II1+(1 - x + floor(x))*II2;
%         elseif (((x - floor(x)) == 0) && ((y - floor(y)) ~= 0))
%             I1 = im(x,y1);
%             I2 = im(x,y2);
%             II1 = im2(x,y1);
%             II2 = im2(x,y2);
%             I = (y - floor(y))*I1+(1 - y + floor(y))*I2;
%             II = (y - floor(y))*II1+(1 - y + floor(y))*II2;
%         else
%             I = im(x,y);
%             II = im2(x,y);
%         end
%         
%         IP1(i,j) = I;%im(Rmax+Ri*cos(2*pi*j/nTh),Rmax+Ri*sin(2*pi*j/nTh));
%         IP2(i,j) = II;%im2(Rmax+Ri*cos(2*pi*j/nTh),Rmax+Ri*sin(2*pi*j/nTh));
%     end
% end


% Thetha Projections

[Theta1, Theta2] = projTheta(IP1,IP2, Rmax);

% Theta1 = zeros(1, size(IP1,2));
% Theta2 = zeros(1, size(IP1,2));
% ntheta = 8*Rmax;
% 
% for j = 1:ntheta
%     tmp3 = 0;
%     tmp4 = 0;
%     for i = 1:Rmax
%         nTh = 8*i;
%         O = ntheta/nTh;
%         n1 = O*(j-1)-floor(O*(j-1));
%         n2 = 1-n1;
%         tmp1 = ceil((j-1)/O);
%         tmp2 = ceil(j/O);
%         if tmp1 ~=0 && tmp2 ~= 0
%             tmp3 = tmp3 + n1*IP1(i,tmp1)+n2*IP1(i,tmp2);
%             tmp4 = tmp4 + n1*IP2(i,tmp1)+n2*IP2(i,tmp2);
%         elseif tmp1 ~=0 && tmp2 == 0
%             tmp3 = tmp3 + n1*IP1(i,tmp1);
%             tmp4 = tmp4 + n1*IP2(i,tmp1);
%         elseif tmp1 ==0 && tmp2 ~= 0
%             tmp3 = tmp3 + n2*IP1(i,tmp2);
%             tmp4 = tmp4 + n2*IP2(i,tmp2);
%         else
%             tmp3 = tmp3+0;
%             tmp4 = tmp4+0;
%         end
%     end
%     Theta1(j) = tmp3;
%     Theta2(j) = tmp4;
% end

T1 = fft(Theta1-mean(Theta1));
T2 = fft(Theta2-mean(Theta2));
t = rad2deg(angle(T1(2)) - angle(T2(2)))
% Angle = 360*t/length(Theta1)

% [acorr,lag] = xcorr(Theta1,Theta2);
% [~,I] = max(acorr);
% d = 360*lag(I)/length(Theta1)
% 
% % Ro projections
% 
% R1 = zeros(1, size(IP1,1));
% R2 = zeros(1, size(IP1,1));
% 
% 
% for i = 1:size(IP1,1)
%     tmp3 = 0;
%     tmp4 = 0;
%     nTh = 8*i;
%     O = ntheta/nTh;
%     for j = 1:nTh
%         tmp3 = tmp3 + O*IP1(i,j);
%         tmp4 = tmp4 + O*IP2(i,j);
%     end
%     R1(i) = tmp3;
%     R2(i) = tmp4;
% end





% % % [V1,ID1] = max(Theta1);
% % % [V2,ID2] = max(Theta2);
% % % 
% % % Angle = 360*(ID2-ID1)/length(Theta1)
% % % 
% % % IM3 = imrotate(im2, -1*Angle);
% % % 
% % % imagesc(IM3)
        
    
    
