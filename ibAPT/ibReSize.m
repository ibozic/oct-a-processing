function im = ibReSize(img, n)

[m,~] = size(img);

IM = fftshift(fft2(img));
IM = padarray(IM, [round(m/2)*n round(m/2)*n],'both');

im = abs(ifft2(ifftshift(IM)));

