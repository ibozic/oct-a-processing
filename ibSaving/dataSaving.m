function dataSaving(dirPath, data1, cFrame, type, par, ibC, data2)
%DATASAVING save images of OCT volumes, even raw and processed
%
% Inputs:
%
%   dirPath - directory path for saving images
%   data1   - first data for save
%   cFrame  - current frame in colume
%   type    - define combination of data
%                 0 - data1 is raw data, no data2 will be used
%                 1 - data1 and data2 are processed data
%                 2 - data1 is processed data, no data2 will be used
%   par     - file name in front of numerical value
%   ibC     - current group of B scans on same position
%   data2   - second data for save

% Author: Ivan Bozic
% Date:   07/17/15
% Cleveland, Ohio, US

if nargin < 7
    data2 = 0;
end

l = 0;

switch type
    case 0
        % disp('Usao 0')
        tmp1  = abs(fftshift(fft(data1),1));
        if size(size(tmp1),2)>=3
            F1 = tmp1(1025:end,:,:);
            l  = size(F1,3);
        else
            F1 = tmp1(1025:end,:);
        end
    case 1
        % disp('Usao 1')
        F1 = data1;
        l  = size(F1,3);
        F2 = data2;
    case 2
        % disp('Usao 2')
        F1 = data1;
        l  = size(F1,3);
    otherwise
        disp('Wrong parameter "type"');
end

if l == 0 && isscalar(data2)
    % disp('Usao I')
    if cFrame < 10
        imwrite(F1/(max(max(max(F1)))), [dirPath '\Scan000' num2str(cFrame) '.tiff']);
    elseif cFrame < 100
        imwrite(F1/(max(max(max(F1)))), [dirPath '\Scan00'  num2str(cFrame) '.tiff']);
    elseif cFrame < 1000
        imwrite(F1/(max(max(max(F1)))), [dirPath '\Scan0'   num2str(cFrame) '.tiff']);
    else
        imwrite(F1/(max(max(max(F1)))), [dirPath '\Scan'    num2str(cFrame) '.tiff']);
    end
elseif l ~= 0 && ~isscalar(data2) && ~isscalar(data1)
    % disp('Usao II')
    if ibC < 10
        imwrite(F2/(max(max(max(F2)))), [dirPath '\' par '00' num2str(ibC) '.tiff']);
    elseif ibC < 100
        imwrite(F2/(max(max(max(F2)))), [dirPath '\' par '0'  num2str(ibC) '.tiff']);
    else
        imwrite(F2/(max(max(max(F2)))), [dirPath '\' par      num2str(ibC) '.tiff']);
    end
    
    for i = 1:l
        if cFrame < 10
            imwrite(F1(:,:,i)/(max(max((F1(:,:,i))))), [dirPath '\' par '_Scan000' num2str(cFrame-l+i) '.tiff']);
        elseif cFrame < 100
            imwrite(F1(:,:,i)/(max(max((F1(:,:,i))))), [dirPath '\' par '_Scan00'  num2str(cFrame-l+i) '.tiff']);
        elseif cFrame < 1000
            imwrite(F1(:,:,i)/(max(max((F1(:,:,i))))), [dirPath '\' par '_Scan0'   num2str(cFrame-l+i) '.tiff']);
        else
            imwrite(F1(:,:,i)/(max(max((F1(:,:,i))))), [dirPath '\' par '_Scan'    num2str(cFrame-l+i) '.tiff']);
        end
    end
    
elseif isscalar(F1)
    if ibC < 10
        imwrite(F2/(max(max(max(F2)))), [dirPath '\' par '00' num2str(ibC) '.tiff']);
    elseif ibC < 100
        imwrite(F2/(max(max(max(F2)))), [dirPath '\' par '0'  num2str(ibC) '.tiff']);
    else
        imwrite(F2/(max(max(max(F2)))), [dirPath '\' par      num2str(ibC) '.tiff']);
    end
    
    
else     
    % disp('Usao III')
    for i = 1:l
        if cFrame < 10
            imwrite(F1(:,:,i)/(max(max((F1(:,:,i))))), [dirPath '\' par '_Scan000' num2str(cFrame-l+i) '.tiff']);
        elseif cFrame < 100
            imwrite(F1(:,:,i)/(max(max((F1(:,:,i))))), [dirPath '\' par '_Scan00'  num2str(cFrame-l+i) '.tiff']);
        elseif cFrame < 1000
            imwrite(F1(:,:,i)/(max(max((F1(:,:,i))))), [dirPath '\' par '_Scan0'   num2str(cFrame-l+i) '.tiff']);
        else
            imwrite(F1(:,:,i)/(max(max((F1(:,:,i))))), [dirPath '\' par '_Scan'    num2str(cFrame-l+i) '.tiff']);
        end
    end
end
    
    
        
    
    
        

        
